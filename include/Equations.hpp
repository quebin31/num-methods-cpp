#pragma once

#include <Tools.hpp>
#include <Matrix.hpp>
#include <Function.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

#include <iostream>

namespace num_methods::equations {
    
    using namespace std;
    using namespace fmt;

    namespace { class NoOutput : public ostream {} no_output; }

    void update_interval(Function &fn, double &a, double &b, double middle) {
        if (fn(a) * fn(middle) < 0)
            b = middle;
        else
            a = middle;
    }

    double point_secant(Function &fn, double a, double b) {
        double fn_a = fn(a);
        double fn_b = fn(b);

        return b - ((fn_b * (b - a))/(fn_b - fn_a));
    }

    double derivative(Function &fn, double x, double delta = 0.0001) {
        expect(fn.get_dim() == 1, "Incorrect dimension, dimension = " + to_string(fn.get_dim()));

        double xd = x + delta;
        return (fn(xd) - fn(x))/delta;
    }

    double partial_derivative(Function &fn, const vector<double>& x, Function::Dimension dim, double delta = 0.0001) {
        expect(dim < fn.get_dim(), 
            "Incorrect dimension, fn dimension = " + to_string(fn.get_dim()) +
            "dimension given = " + to_string(dim)
        );

        expect(x.size() == fn.get_dim(), "x point differs from dimension, fn dimension = " + to_string(fn.get_dim()));

        Matrix canonical_e(Matrix<double>::zero(1, x.size()));
        canonical_e(0, dim) = 1;
        Matrix xd = Matrix(x) + (canonical_e * delta);
        return (fn(xd(0)) - fn(x))/delta;
    }

    double newton_aproximation_formula(Function &fn, double pos_solution) {
        double dfn = derivative(fn, pos_solution);
        return pos_solution - (fn(pos_solution) / dfn);
    }

    double newton_aproximation_formula(Function &fn, Function &dfn, double pos_solution) {
        return pos_solution - (fn(pos_solution)/dfn(pos_solution));
    }

    double secant_method_formula(Function &fn, double r0, double r1) {
        return r1 - (fn(r1) * (r1 - r0) / (fn(r1) - fn(r0)));
    }

    double bisection_with_steps(Function &fn, double a, double b, int steps, bool verbose = false,
                                                            ostream &out = cout) {
        double pos_solution = 0;
        print(verbose? out : no_output, "k, a, b, p, f(p)\n");

        for (int ii = 0; ii < steps; ++ii) {
            pos_solution = (a + b) / 2;
            print(verbose? out : no_output, "{}, {:.6f}, {:.6f}, {:.6f}, {:.6f}\n", ii+1, a, b, pos_solution, fn(pos_solution));
            update_interval(fn, a, b, pos_solution);
        }

        return pos_solution;
    }


    double bisection_with_img_tol(Function &fn, double a, double b, double tolerance,
                                                                bool verbose = false, ostream &out = cout) {
        double pos_solution     = 0;
        double solution_image = 100;
        print(verbose? out : no_output, "k, a, b, p, | f(p) |\n");

        for (int ii = 0; comp::ge(solution_image, tolerance, 6); ++ii) {
            pos_solution     = (a + b) / 2;
            solution_image = std::fabs(fn(pos_solution));
            print(verbose? out : no_output, "{}, {:.6f}, {:.6f}, {:.6f}, {:.6f}\n", ii+1, a, b, pos_solution, solution_image);
            update_interval(fn, a, b, pos_solution);
        }

        return pos_solution;
    }

    double bisection_with_int_tol(Function &fn, double a, double b, double tolerance,
                                                                bool verbose = false, ostream &out = cout) {
        double pos_solution = 0;
        double int_distance = fabs(a - b);
        print(verbose? out : no_output, "k, a, b, p , | a - b |, f(p)\n");

        for (int ii = 0; comp::ge(int_distance, tolerance, 6); ++ii) {
            pos_solution = (a + b) / 2;
            int_distance = fabs(a - b);
            print(verbose? out : no_output, "{}, {:.6f}, {:.6f}, {:.6f}, {:.6f}, {:.6f}\n", ii+1, a, b, pos_solution, int_distance, fn(pos_solution));
            update_interval(fn, a, b, pos_solution);
        }

        return pos_solution;
    }

    double regula_with_steps(Function &fn, double a, double b, int steps, bool verbose = false,
                                                     ostream &out = cout) {

        double pos_solution = 0;
        print(verbose? out : no_output, "k, a, b, r, f(r)");

        for (int ii = 0; ii < steps; ++ii) {
            pos_solution = point_secant(fn, a, b);
            print(verbose? out : no_output, "{}, {:.6f}, {:.6f}, {:.6f}, {:.6f}\n", ii+1, a, b, pos_solution, fn(pos_solution));
            update_interval(fn, a, b, pos_solution);
        }

        return pos_solution;
    }

    double regula_with_img_tol(Function &fn, double a, double b, double tolerance,
                                                         bool verbose = false, ostream &out = cout) {

        double pos_solution     = 0;
        double solution_image = 100;
        print(verbose? out : no_output, "k, a, b, r, | f(r) |\n");

        for (int ii = 0; comp::ge(solution_image, tolerance, 6); ++ii) {
            pos_solution     = point_secant(fn, a, b);
            solution_image = fabs(fn(pos_solution));
            print(verbose? out : no_output, "{}, {:.6f}, {:.6f}, {:.6f}, {:.6f}\n", ii+1, a, b, pos_solution, solution_image);
            update_interval(fn, a, b, pos_solution);
        }

        return pos_solution;
    }

    double regula_with_err_tol(Function &fn, double a, double b, double tolerance,
                                                         bool verbose = false, ostream &out = cout) {

        double pos_solution  = 0;
        double last_solution = 0;
        double solution_dist = 100;
        print(verbose? out : no_output, "k, a, b, r, | r_k - r_k-1 |, f(r)\n");

        for (int ii = 0; comp::ge(solution_dist, tolerance, 6); ++ii) {
            pos_solution    = point_secant(fn, a, b);
            solution_dist = fabs(pos_solution - last_solution);
            print(verbose? out : no_output, "{}, {:.6f}, {:.6f}, {:.6f}, {:.6f}, {:.6f}\n", ii+1, a, b, pos_solution, solution_dist, fn(pos_solution));
            last_solution = pos_solution;
            update_interval(fn, a, b, pos_solution);
        }

        return pos_solution;
    }

    double newton_with_steps(Function &fn, double aproximation, int steps, bool verbose = false,
                                                     ostream &out = cout) {

        double pos_solution = 0;
        print(verbose? out : no_output, "k, r, f(r)\n");

        for (int ii = 0; ii < steps; ++ii) {
            pos_solution = newton_aproximation_formula(fn, aproximation);
            print(verbose? out : no_output, "{}, {:.6f}, {:.6f}\n", ii+1, pos_solution, fn(pos_solution));
            aproximation = pos_solution;
        }

        return pos_solution;
    }

    double newton_with_steps(Function &fn, Function &dfn, double aproximation, int steps, bool verbose = false,
                                                     ostream &out = cout) {

        double pos_solution = 0;
        print(verbose? out : no_output, "k, r, f(r)\n");

        for (int ii = 0; ii < steps; ++ii) {
            pos_solution = newton_aproximation_formula(fn, dfn, aproximation);
            print(verbose? out : no_output, "{}, {:.6f}, {:.6f}\n", ii+1, pos_solution, fn(pos_solution));
            aproximation = pos_solution;
        }

        return pos_solution;
    }

    double newton_with_img_tol(Function &fn, double aproximation, double tolerance, bool verbose = false,
                                                         ostream &out = cout) {

        double pos_solution     = 0;
        double solution_image = 100;
        print(verbose? out : no_output, "k, r, | f(r) |\n");

        for (int ii = 0; comp::ge(solution_image, tolerance, 6); ++ii) {
            pos_solution     = newton_aproximation_formula(fn, aproximation);
            solution_image = fabs(fn(pos_solution));
            print(verbose? out :    no_output, "{}, {:.6f}, {:.6f}\n", ii+1, pos_solution, solution_image);
            aproximation = pos_solution;
        }

        return pos_solution;
    }

    double newton_with_img_tol(Function &fn, Function &dfn, double aproximation, double tolerance, bool verbose = false,
                                                         ostream &out = cout) {

        double pos_solution     = 0;
        double solution_image = 100;
        print(verbose? out : no_output, "k, r, | f(r) |\n");

        for (int ii = 0; comp::ge(solution_image, tolerance, 6); ++ii) {
            pos_solution     = newton_aproximation_formula(fn, dfn, aproximation);
            solution_image = fabs(fn(pos_solution));
            print(verbose? out : no_output, "{}, {:.6f}, {:.6f}\n", ii+1, pos_solution, solution_image);
            aproximation = pos_solution;
        }

        return pos_solution;
    }

    double newton_with_err_tol(Function &fn, double aproximation, double tolerance, bool verbose = false,
                                                         ostream &out = cout) {

        double pos_solution  = 0;
        double solution_dist = 100;
        print(verbose? out : no_output, "k, r, | r_k - r_k-1 |, f(r)\n");

        for (int ii = 0; comp::ge(solution_dist, tolerance, 6); ++ii) {
            pos_solution    = newton_aproximation_formula(fn, aproximation);
            solution_dist = fabs(pos_solution - aproximation);
            print(verbose? out : no_output, "{}, {:.6f}, {:.6f}, {:.6f}\n", ii+1, pos_solution, solution_dist, fn(pos_solution));
            aproximation = pos_solution;
        }

        return pos_solution;
    }

    double newton_with_err_tol(Function &fn, Function &dfn, double aproximation, double tolerance, bool verbose = false,
                                                         ostream &out = cout) {

        double pos_solution  = 0;
        double solution_dist = 100;
        print(verbose? out : no_output, "k, r, | r_k - r_k-1 |, f(r)\n");

        for (int ii = 0; comp::ge(solution_dist, tolerance, 6); ++ii) {
            pos_solution    = newton_aproximation_formula(fn, dfn, aproximation);
            solution_dist = fabs(pos_solution - aproximation);
            print(verbose? out : no_output, "{}, {:.6f}, {:.6f}, {:.6f}\n", ii+1, pos_solution, solution_dist, fn(pos_solution));
            aproximation = pos_solution;
        }

        return pos_solution;
    }

    double secant_with_steps(Function &fn, double aproximation0, double aproximation1, int steps, bool verbose = false,
                                                     ostream &out = cout) {
        double pos_solution = 0;
        print(verbose? out : no_output, "k, r, f(r)\n");

        for (int ii = 0; ii <= steps; ++ii) {
            pos_solution = secant_method_formula(fn, aproximation0, aproximation1);
            print(verbose? out : no_output, "{}, {:.6f}, {:.6f}", ii+1, pos_solution, fn(pos_solution));
            aproximation0 = aproximation1;
            aproximation1 = pos_solution;
        }

        return pos_solution;
    }

    double secant_with_img_tol(Function &fn, double aproximation0, double aproximation1, double tolerance, bool verbose = false,
                                                         ostream &out = cout) {

        double pos_solution     = 0;
        double solution_image = 100;
        print(verbose? out : no_output, "k, r, | f(r) |\n");

        for (int ii = 0; comp::ge(solution_image, tolerance, 6); ++ii) {
            pos_solution     = secant_method_formula(fn, aproximation0, aproximation1);
            solution_image = fabs(fn(pos_solution));
            print(verbose? out : no_output, "{}, {:.6f}, {:.6f}", ii+1, pos_solution, solution_image);
            aproximation0 = aproximation1;
            aproximation1 = pos_solution;
        }

        return pos_solution;
    }

    double secant_with_err_tol(Function &fn, double aproximation0, double aproximation1, double tolerance, bool verbose = false,
                                                         ostream &out = cout) {

        double pos_solution  = 0;
        double solution_dist = 100;
        print(verbose? out : no_output, "k, r_k-1, r_k, | r_k - r_k-1 |, f(r_k)\n");

        for (int ii = 0; comp::ge(solution_dist, tolerance, 6); ++ii) {
            pos_solution    = secant_method_formula(fn, aproximation0, aproximation1);
            solution_dist = fabs(pos_solution - aproximation1);
            print(verbose? out :    no_output, "{}, {:.6f}, {:.6f}, {:.6f}, {:.6f}\n", ii+1, aproximation1, pos_solution, solution_dist, fn(pos_solution));
            aproximation0 = aproximation1;
            aproximation1 = pos_solution;
        }

        return pos_solution;
    }

    enum ConvAlternativeMethod : int {
        FIRST, SECOND, THIRD
    };

    double convergence(Function &fn, double n, double limit, ConvAlternativeMethod alt = FIRST) {
        n -= 2; double f_na = fn(n);
        n += 1; double f_n0 = fn(n);
        n += 1; double f_n1 = fn(n);
        n += 1; double f_n2 = fn(n);

        double num = 0.0;
        double den = 0.0;

        if (alt == FIRST) {
            num = log(fabs(f_n2 - limit) / fabs(f_n1 - limit));
            den = log(fabs(f_n1 - limit) / fabs(f_n0 - limit));
        } else if (alt == SECOND) {
            num = log(fabs(f_n2 - limit)) - log(fabs(f_n1 - limit));
            den = log(fabs(f_n1 - limit)) - log(fabs(f_n0 - limit));
        } else if (alt == THIRD) {
            num = log(fabs(f_n2 - f_n1) / fabs(f_n1 - f_n0));
            den = log(fabs(f_n1 - f_n0) / fabs(f_n0 - f_na));
        } else { throw std::runtime_error("Unknown alternative"); }

        return num / den;
    }

    double convergence_speed(Function &fn, double n, double limit, double tolerance = 0.0001, ConvAlternativeMethod alt = FIRST) {
        double error = 100;
        double lasta = 0;
        double alpha = 0;

        for (double ii = n; error > tolerance; ++ii) {
            alpha = convergence(fn, ii, limit, alt);
            error = alpha - lasta;
            print("{:.6f}\n", alpha);
            lasta = alpha;
        }

        return alpha;
    }

    using StaticFunction = std::function<double (double)>;

    double convergence(const StaticFunction &fn, double n, double limit, ConvAlternativeMethod alt = FIRST) {
        n -= 2; double f_na = fn(n);
        n += 1; double f_n0 = fn(n);
        n += 1; double f_n1 = fn(n);
        n += 1; double f_n2 = fn(n);

        double num = 0.0;
        double den = 0.0;

        if (alt == FIRST) {
            num = log(fabs(f_n2 - limit) / fabs(f_n1 - limit));
            den = log(fabs(f_n1 - limit) / fabs(f_n0 - limit));
        } else if (alt == SECOND) {
            num = log(fabs(f_n2 - limit)) - log(fabs(f_n1 - limit));
            den = log(fabs(f_n1 - limit)) - log(fabs(f_n0 - limit));
        } else if (alt == THIRD) {
            num = log(fabs(f_n2 - f_n1) / fabs(f_n1 - f_n0));
            den = log(fabs(f_n1 - f_n0) / fabs(f_n0 - f_na));
        } else { throw std::runtime_error("Unknown alternative"); }

        return num / den;
    }

    double convergence_speed(const StaticFunction &fn, double n, double limit, double tolerance = 0.0001, ConvAlternativeMethod alt = FIRST) {
        double error = 100;
        double lasta = 0;
        double alpha = 0;

        for (double ii = n; comp::ge(error, tolerance, 6); ++ii) {
            alpha = convergence(fn, ii, limit, alt);
            error = alpha - lasta;
            print("{:.6f}\n", alpha);
            lasta = alpha;
        }

        return alpha;
    }

}
