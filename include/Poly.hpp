#pragma once

#include <Tools.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

#include <cmath>
#include <vector>
#include <utility>

namespace num_methods {

    using namespace std;

    template <class T>
    constexpr void check_arithmetic() {
        static_assert(is_arithmetic<T>::value && !is_pointer<T>::value, "Type isn't arithmetic");
    }
    
    template <class T>
    class Poly {
    private:
        vector<T> poly;

    public:
        Poly(): poly() { check_arithmetic<T>(); }
        Poly(const Poly& poly): poly(poly.poly) { check_arithmetic<T>(); }
        Poly(size_t grade, T def = 0): poly(grade + 1, def) { check_arithmetic<T>(); }
        explicit Poly(vector<T> vec): poly(move(vec)) { check_arithmetic<T>(); }

        T operator()(T val) {
            T sum = 0;        

            for (int i = 0; i < poly.size(); ++i)
                sum += poly[i] * pow(val, poly.size() - i - 1);

            return sum;
        }

        Poly operator+(const Poly& rhs) {
            Poly result(poly);
            expect(poly.size() == rhs.poly.size(), "Grade differ");
            for (int i = 0; i < result.poly.size(); ++i) 
                result.poly[i] = poly[i] + rhs.poly[i];

            return result;
        }

        Poly operator-(const Poly& rhs) {
            Poly result(poly);
            expect(poly.size() == rhs.poly.size(), "Grade differ");
            for (int i = 0; i < result.poly.size(); ++i) 
                result.poly[i] = poly[i] - rhs.poly[i];

            return result;
        }

        Poly operator*(T val) {
            Poly result(poly);
            for (auto& term : result.poly)
                term *= val;
            return result;
        }

        Poly operator*(const Poly& rhs) {
            size_t m = poly.size();
            size_t n = rhs.poly.size();
            size_t k = m + n - 1;

            Poly result(k - 1);
            for (int i = 0; i < poly.size(); ++i) {
                for (int j = 0; j < rhs.poly.size(); ++j) {
                    result.poly[i + j] += poly[i] * rhs.poly[j];
                }
            }

            return result;
        }

        Poly operator/(T val) {
            Poly result(poly);
            for (auto& term : result.poly)
                term /= val;
            return result;
        }

        void increase_grade_to(size_t grade) {
            expect(grade >= poly.size() - 1, "Grade should be greater");
            vector<T> new_poly(grade + 1, 0);

            int diff = new_poly.size() - poly.size();
            for (int i = 0; i < poly.size(); ++i)
                new_poly[i + diff] = poly[i];

            poly = new_poly;
        }

        friend ostream& operator<<(ostream& out, const Poly& poly) {
            const vector<T>& vec = poly.poly;
            for (int i = 0; i < vec.size(); ++i) 
                out << vec[i] << ' ';
            
            return out;
        }
    };

}
