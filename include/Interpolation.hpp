#pragma once

#include <Poly.hpp>
#include <Tools.hpp>
#include <Matrix.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

namespace num_methods {

    using namespace std;
    using namespace fmt;

    Poly<double> lagrange_interpolation(const vector<double>& x, const vector<double>& y) {
        expect(x.size() == y.size(), format("x size is {} and y size is {}", x.size(), y.size()));
        size_t no_points = x.size();

        double         denom = 1;
        Poly<double>   result(no_points - 1, 0);
        Poly<double>   tmppol(0, 1);
        vector<double> tmpvec(2);

        for (auto i = 0UL; i < no_points; ++i) {
            denom  = 1;
            tmppol = Poly<double>(0, 1);

            for (int j = 0; j < no_points; ++j) {
                if (i == j) continue;
                tmpvec[0] = 1;
                tmpvec[1] = -x[j];

                tmppol = tmppol * Poly<double>(tmpvec);
                denom *= (x[i] - x[j]);
            }

            tmppol = tmppol * (y[i] / denom);
            result = result + tmppol;
        }
    
        return result;
    }


    Poly<double> newton_interpolation(const vector<double>& x, const vector<double>& y) {
        expect(x.size() == y.size(), format("x size is {} and y size is {}!", x.size(), y.size()));

        size_t no_points = x.size();
        Poly<double>   result(no_points - 1, 0);
        Poly<double>   tmppol(0, 1);
        vector<double> tmpvec(2);

        Matrix f_mat(Matrix<double>::zero(no_points, no_points + 1));
        for (auto i = 0UL; i < no_points; ++i) {
            f_mat(i, 0) = x[i];
            f_mat(i, 1) = y[i];
        }

        for (auto j = 2UL; j < no_points + 1; ++j) {
            for (auto i = j - 1; i < no_points; ++i) {
                double num = f_mat(i, j - 1) - f_mat(i - 1, j - 1);
                double den = f_mat(i, 0) - f_mat(i - j + 1, 0);
                f_mat(i,j) = num / den;
            }
        }

        for (auto i = 0UL; i < no_points; ++i) {
            tmppol = Poly<double>(0, 1);

            for (auto j = 0UL; j < i; ++j) {
                tmpvec[0] = 1;
                tmpvec[1] = -x[j];
                tmppol = tmppol * Poly<double>(tmpvec);
            }

            tmppol = tmppol * f_mat(i, i + 1);
            tmppol.increase_grade_to(no_points - 1);
            result = result + tmppol;
        }

        return result;
    }
}
