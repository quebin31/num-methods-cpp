#pragma once

#include <cmath>
#include <string>
#include <vector>
#include <fparser.hh>

#include <Tools.hpp>

namespace num_methods {

    using namespace std;

    class Function {
    public:
        using Dimension = unsigned short;

    private:
        Dimension      dimension;
        FunctionParser fun;

    public:
        Function(): dimension(0) {
            fun.AddConstant("pi", 3.141592653589793);
            fun.AddConstant("e" , 2.718281828459045);
        }

        Function(const Function&) = default;
        Function(string function, string vars) {
            fun.AddConstant("pi", 3.141592653589793);
            fun.AddConstant( "e", 2.718281828459045);
            if (fun.Parse(function, vars) >= 0)
                throw std::runtime_error("Failed to parse function");

            dimension = parse_number_of_vars(vars);
        }

        void parse(string function, string vars) {
            if (fun.Parse(function, vars) >= 0)
                throw std::runtime_error("Failed to parse function");

            dimension = parse_number_of_vars(vars);
        }

        double operator()(const vector<double>& vals) {
            expect(dimension != 0, "Cannot call an unitialized function");
            const double* valsp = vals.data();
            return fun.Eval(valsp); 
        }

        double operator()(double a) { 
            expect(dimension != 0, "Cannot call an unitialized function");
            return fun.Eval(&a); 
        }

        unsigned short get_dim() const { return dimension; }

    private:

        Dimension parse_number_of_vars(string vars) {
            size_t pos = 0;
            unsigned short dim = 0;

            while ((pos = vars.find(",")) != string::npos) {
                vars.substr(0, pos);
                vars.erase(0, pos + 1);
                dim += 1;
            }

            if (vars.size() != 0) 
                dim += 1;

            return dim;
        }
    };

}
