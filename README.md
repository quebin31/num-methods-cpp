# Numerical Methods 

## Compiling

### Under Linux 

- Dependencies (build)
    - cmake (>= 3.9)

- Steps 

    ``` 
    $ git clone https://gitlab.com/lans98/num-methods-cpp
    $ cd num-methods-cpp
    $ mkdir build 
    $ cd build 
    $ cmake .. 
    $ cmake --build .
    ```

### Under Windows 

- Dependencies (build)
    - cmake (>= 3.9)

- Steps with cmake 

    ```
    //TODO 
    ```
