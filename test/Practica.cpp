#include <NumMethods.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

using namespace std;
using namespace fmt;
using namespace num_methods;
using namespace num_methods::equations;
using namespace num_methods::eqsystems;

void ejercicio1() {
    print("\nEjercicio 1:\n");
    FunctionParser fparser;
    Function fun("x^3 * e^(-x) + 2*x - 4", "x");
    auto root = secant_with_err_tol(fun, 10, 20, 0.001, true);
    print("Final root: {}\n", root);
}

void ejercicio2() {
    print("\nEjercicio 2:\n");
    Matrix<double> c({
        { 3, 1, -1, 1 },
        { 1, -4, 1, -1 },
        { -4, 1, 1, 4 },
        { -2, 2, 1, -3 }
    });

    Matrix<double> i(4,1);
    i(0,0) = 4;
    i(1,0) = 5;
    i(2,0) = 8;
    i(3,0) = 21;

    auto solutions = gauss_advanced(c, i);
    print("Solutions: ");
    for (auto& s: solutions)
        print("{:>10.6f} ", s);
    print("\n");
}

void ejercicio3() {
    print("\nEjercicio 3:\n");
    Matrix<double> c({
        { 4, -1, -1 },
        { -1, 3, -1 },
        { 1, 1, 4 }
    });

    Matrix<double> i(3,1);
    i(0,0) = 4;
    i(1,0) = 3;
    i(2,0) = 4;

    auto solutions = gauss_seidel(c, i, {0,0,0}, 0.0001);
    print("Solutions: ");
    for (auto& s: solutions)
        print("{:>10.6f} ", s);
    print("\n");
}

int main() {
    ejercicio1();
    ejercicio2();
    ejercicio3();
}
