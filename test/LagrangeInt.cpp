#include <NumMethods.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

using namespace std;
using namespace fmt;
using namespace num_methods;

void test1() {
    print("Case 1:\n");
    vector<double> x = { 2, 3, 5 };
    vector<double> y = { 4, 1, 2 };
    auto poly = lagrange_interpolation(x,y);
    print("Poly: {}\n", poly);

    vector<double> a;
    for (auto& e:x)
        a.push_back(poly(e));

    print("Original y's: ");
    for (auto& e:y)
        print("{} ", e);
    
    print("\nSolved y's: ");
    for (auto& e:a)
        print("{} ", e);
    print("\n\n");
}

void test2() {
    print("Case 2:\n");
    vector<double> x = {  2, 4,  6, 7 };
    vector<double> y = { -1, 2, -3, 0 };
    auto poly = lagrange_interpolation(x,y);
    print("Poly: {}\n", poly);

    vector<double> a;
    for (auto& e:x)
        a.push_back(poly(e));
    
    print("Original y's: ");
    for (auto& e:y)
        print("{} ", e);

    print("\nSolved y's: ");
    for (auto& e:a)
        print("{} ", e);
    print("\n\n");
}

int main() {
    test1();
    test2();
}
