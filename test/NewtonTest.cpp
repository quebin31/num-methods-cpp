#include <NumMethods.hpp>

using namespace std;
using namespace fmt;
using namespace num_methods;
using namespace num_methods::equations;

int main() {
    Function fun;
    string func1 = "x^3+4*x^2-10";
    string func2 = "1/2*x^2+x+1-e^x";

    double aprox1 = 1.5;
    double aprox2 = 1.0;
    double tolerance = 0.0001;

    fun.parse(func1, "x");
    print("\nFinding root for equation: {} with newton method\n", func1);
    auto root1 = newton_with_err_tol(fun, aprox1, tolerance, true);

    fun.parse(func2, "x");
    print("\nFinding root for equation: {} with newton method\n", func2);
    auto root2 = newton_with_err_tol(fun, aprox2, tolerance, true);
}
