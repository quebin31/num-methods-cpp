#include <NumMethods.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

using namespace std;
using namespace fmt;
using namespace num_methods;
using namespace num_methods::eqsystems;

void test_case(const string& casetit, const Matrix<double>& a, const Matrix<double>& b, const vector<double>& aproximations, double tolerance) {
    auto solutions = Matrix<double>(jacobi_method(a,b, aproximations, tolerance)).transpose();
    print("{}\n", casetit);
    print("Solutions:\n{}", solutions);
    print("A * X:\n{}B:\n{}", a * solutions, b);
}

int main() {
    {
        Matrix<double> coeficients({
            {  5, -2,  1,  1},
            { -1,  4,  1, -1},
            {  2, -2,  7, -1},
            {  0,  1,  3, -6}
        });

        Matrix<double> independents(4, 1);
        independents(0,0) = 1;
        independents(1,0) = 2;
        independents(2,0) = 3;
        independents(3,0) = 4;

        vector<double> aproximations = { 0, 0, 0, 0 };
        test_case("Case 1: ", coeficients, independents, aproximations, 0.0000001);
    }

    {
        Matrix<double> coeficients({
            {  1,  2,  3,  4},
            { -2, -1,  1,  1},
            {  4,  3,  1,  0},
            { -1,  1,  2,  1}
        });

        Matrix<double> independents(4, 1);
        independents(0,0) = 1;
        independents(1,0) = 2;
        independents(2,0) = 3;
        independents(3,0) = 4;

        vector<double> aproximations = { 0, 0, 0, 0 };
        test_case("Case 2: ", coeficients, independents, aproximations, 0.0000001);
    }
}
