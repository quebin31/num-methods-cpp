#include <NumMethods.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

using namespace std;
using namespace fmt;
using namespace num_methods;
using namespace num_methods::eqsystems;

int main() {
    { // Gauss simple and advanced, LU and PLU
        Matrix<double> coeficients({
            {  2, 3,     4, -5 },
            { -3, 4,    -5,  7 },
            {  5, 1,    -1,  6 },
            {  7, 9, -13, 17 }
        });

        Matrix<double> independents(4, 1);
        independents(0, 0) = 7;
        independents(1, 0) = 2;
        independents(2, 0) = 5;
        independents(3, 0) = 21;

        auto eq_system = coeficients.augmented(independents);
        print("eq_system:\n{}", eq_system);

        { //    Gauss Simple
            auto solutions = gauss_simple(eq_system);
            print("solutions using gauss simple: ");
            for (auto &sol : solutions)
                print("{} ", sol);
        }

        { // Gauss Advanced
            auto solutions = gauss_advanced(eq_system);
            print("solutions using gauss simple: ");
            for (auto &sol : solutions)
                print("{} ", sol);
        }

        { // LU method
            auto solutions = lu_method(coeficients, independents);
            print("Solutions using lu method: ");
            for (auto &sol : solutions)
                print("{} ", sol);
        }

        { // PLU method
            auto solutions = plu_method(coeficients, independents);
            print("Solutions using plu method: ");
            for (auto &sol : solutions)
                print("{} ", sol);
        }
    }
}
