#include <NumMethods.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

using namespace fmt;
using namespace num_methods;
using namespace num_methods::equations;
using namespace num_methods::eqsystems;

void single_variable() {
    Function fun1("x * 3", "x");
    Function fun2("x ^ 3", "x");
    
    expect(fun1.get_dim() == 1, "Error detecting dimension");
    expect(fun2.get_dim() == 1, "Error detecting dimension");

    double dfun1 = derivative(fun1, 1);
    double dfun2 = derivative(fun2, 2);
    expect(comp::eq(dfun1,  3.0, 1), "Expected 3.0, got " + to_string(dfun1));
    expect(comp::eq(dfun2, 12.0, 1), "Expected 12.0, got " + to_string(dfun2));
}

void multi_variable() {
    Function fun("x ^ 3 + 2 * y ^ 2 - 3 * z", "x,y,z");

    expect(fun.get_dim() == 3, "Error detecting dimension");

    vector<double> point = { 1, 1, 1 };
    double dfunx = partial_derivative(fun, point, 0);
    double dfuny = partial_derivative(fun, point, 1);
    double dfunz = partial_derivative(fun, point, 2);

    expect(comp::eq(dfunx,  3.0, 1), "Expected 3.0, got " + to_string(dfunx));
    expect(comp::eq(dfuny,  4.0, 1), "Expected 4.0, got " + to_string(dfunx));
    expect(comp::eq(dfunz, -3.0, 1), "Expected -3.0, got " + to_string(dfunx));
}

void vector_single_variable() {
    FunctionMatrix fun(3,1);
    fun(0,0) = Function("x * 3", "x");
    fun(1,0) = Function("x ^ 3", "x");
    fun(2,0) = Function("3 * x ^ 2", "x");

    vector<double> v(1);
    v[0] = 1;

    auto jacobian = calculate_jacobian(fun, v);
    expect(comp::eq(jacobian(0,0),  3.0, 1), "Expected 3.0, got " + to_string(jacobian(0,0)));
    expect(comp::eq(jacobian(1,0),  3.0, 1), "Expected 3.0, got " + to_string(jacobian(1,0)));
    expect(comp::eq(jacobian(2,0),  6.0, 1), "Expected 6.0, got " + to_string(jacobian(2,0)));
    print("\n{}", jacobian);
}

void vector_multi_variable() {
    FunctionMatrix fun(2,1);
    fun(0,0) = Function("x * 3 + y ^ 2", "x,y");
    fun(1,0) = Function("x ^ 2 + 2 * y ^ 3", "x,y");

    vector<double> v(2);
    v[0] = 1;
    v[1] = 1;

    auto jacobian = calculate_jacobian(fun, v);
    expect(comp::eq(jacobian(0,0),  3.0, 1), "Expected 3.0, got " + to_string(jacobian(0,0)));
    expect(comp::eq(jacobian(0,1),  2.0, 1), "Expected 2.0, got " + to_string(jacobian(0,0)));
    expect(comp::eq(jacobian(1,0),  2.0, 1), "Expected 2.0, got " + to_string(jacobian(0,0)));
    expect(comp::eq(jacobian(1,1),  6.0, 1), "Expected 6.0, got " + to_string(jacobian(0,0)));
    print("\n{}", jacobian);
}

int main() {
    single_variable();
    multi_variable();
    vector_single_variable();
    vector_multi_variable();
}
