#include <NumMethods.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

using namespace std;
using namespace fmt;
using namespace num_methods;
using namespace num_methods::eqsystems;

#define print_solutions(header, sol) \
    print("{}", header);\
    for (auto& s : sol)\
        print("{:.6f} ", s);\
    print("\n");


void test_case(const Matrix<double>& a, const Matrix<double>& b) {
        try { // LU
            auto solutions = lu_method(a, b);
            print_solutions("LU: ", solutions);
        } catch (const exception& e) {
            print("This eq system doesn't have LU decomposition {{ exception: {} }}\n", e.what());
        }

        { // PLU
            auto solutions = plu_method(a, b);
            print_solutions("PLU: ", solutions);
        }
}

int main() {
    {
        Matrix<double> coeficients({
            {  0,  0, -1, 1},
            {  1,  1, -1, 2},
            { -1, -1,  2, 0},
            {  1,  2,  0, 0}
        });

        Matrix<double> independents(4, 1);
        independents(0,0) = 4;
        independents(1,0) = 3;
        independents(2,0) = 2;
        independents(3,0) = 1;

        test_case(coeficients, independents);
    }

    {
        Matrix<double> coeficients({
            {  1, -1,  2, 2},
            {  2, -2,  1, 1},
            { -2, -1,  0, 1},
            {  3,  2, -1, 1}
        });

        Matrix<double> independents(4, 1);
        independents(0,0) = 1;
        independents(1,0) = 2;
        independents(2,0) = 3;
        independents(3,0) = 4;

        test_case(coeficients, independents);
    }

    {
        Matrix<double> coeficients({
            {  1,  2,  3,  4},
            {  5,  6,  7,  8},
            {  9, 10, 11, 12},
            { 13, 14, 15, 16}
        });

        Matrix<double> independents(4, 1);
        independents(0,0) = 1;
        independents(1,0) = 2;
        independents(2,0) = 3;
        independents(3,0) = 4;

        test_case(coeficients, independents);
    }

    {
        Matrix<double> coeficients({
            { 1, 2, 3, 4},
            { 4, 3, 2, 1},
            { 1, 2, 2, 2},
            { 1, 1, 1, 1}
        });

        Matrix<double> independents(4, 1);
        independents(0,0) = 1;
        independents(1,0) = 1;
        independents(2,0) = 1;
        independents(3,0) = 1;

        test_case(coeficients, independents);
    }

    {
        Matrix<double> coeficients({
            {  2, 3,     4, -5},
            { -3, 4,    -5,  7},
            {  5, 1,    -1,  6},
            {  7, 9, -13, 17}
        });

        Matrix<double>independents(4, 1);
        independents(0,0) = 1;
        independents(1,0) = 1;
        independents(2,0) = 1;
        independents(3,0) = 1;

        test_case(coeficients, independents);
    }
}
