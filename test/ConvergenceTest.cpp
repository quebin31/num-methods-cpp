#include <NumMethods.hpp>

using namespace std;
using namespace fmt;
using namespace num_methods;
using namespace num_methods::equations;

int main() {
    Function fun;
    string func1 = "1/3^x";
    string func2 = "2/3^(2^x)";
    string func3 = "1/x^2";

    fun.parse(func1, "x");
    print("\nAproximation to the convergence value for: {}\n", func1);
    auto alpha1 = convergence_speed(fun, 2, 0);

    fun.parse(func2, "x");
    print("\nAproximation to the convergence value for: {}\n", func2);
    auto alpha2 = convergence_speed(fun, 2, 0);

    fun.parse(func3, "x");
    print("\nAproximation to the convergence value for: {}\n", func3);
    auto alpha3 = convergence_speed(fun, 2, 0);

    print("\nAproximation to the convergence value for: x_(n) = 1/2*x_(n-1) + 1/x_(n-1)\n");
    auto alpha4 = convergence_speed([](double x){
        StaticFunction recurse;

        recurse = [&](double x) -> double {
            if (comp::eq(x, 0.0, 4))
                return 2;

            double r = recurse(x - 1);
            return 0.5*r + 1/r;
        };

        return recurse(x);
    }, 2, 0, 0.001, THIRD);

    print("\nAproximation to the convergence value for: x^3+4*x^2-10 using newton formula\n");
    auto alpha5 = convergence_speed([](double x){
            StaticFunction recurse;

            recurse = [&](double x) -> double {
                if (comp::eq(x, 0.0, 4))
                    return 1.5;

                double r = recurse(x - 1);
                return (2*pow(r, 3) + 4*r*r + 10) / (3*r*r + 8*r);
            };

            return recurse(x);
    }, 2, 0, 0.0001, THIRD);

    print("\nAproximation to the convergence value for: 1/2*x^2+x+1-e^x using newton formula\n");
    auto alpha6 = convergence_speed([](double x){
            StaticFunction recurse;

            recurse = [&](double x) -> double {
                if (comp::eq(x, 0.0, 4))
                    return 1;

                double r = recurse(x - 1);
                return (0.5*r*r - 1 - exp(r) * (r - 1)) / (r + 1 - exp(r));
            };

            return recurse(x);
    }, 2, 0, 0.0001, THIRD);
}
